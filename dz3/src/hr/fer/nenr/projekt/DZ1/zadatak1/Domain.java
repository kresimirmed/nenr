package hr.fer.nenr.projekt.DZ1.zadatak1;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;

public abstract class Domain implements IDomain{

	public Domain() {
		
	}
	
	public static IDomain intRange(int first, int last) {
		return new SimpleDomain(first,last);
	}
	
	public static Domain combine(IDomain first, IDomain second) {
		SimpleDomain[] list= new SimpleDomain[0];
		if(first instanceof SimpleDomain){
			
			SimpleDomain[] pom = {(SimpleDomain) first};
			list=joinArrays(list,pom);
		}else {
			SimpleDomain[] sDList = ((CompositeDomain)first).getSimpleDomain();
			list=joinArrays(list, sDList);
		}
		if  (second instanceof SimpleDomain){
			SimpleDomain[] pom = {(SimpleDomain) second};
			list=joinArrays(list,pom);
		}
		else{
			SimpleDomain[] sDList = ((CompositeDomain)second).getSimpleDomain();
			list=joinArrays(list, sDList);
		}
		
		return new CompositeDomain(list);
	}
	
	public abstract int indexOfElement(DomainElement element);
	
	public abstract DomainElement elementForIndex(int index) ;
	
	private static SimpleDomain[] joinArrays(SimpleDomain[] a, SimpleDomain[] b) {
		SimpleDomain[] r = new SimpleDomain[a.length + b.length];
        System.arraycopy(a, 0, r, 0, a.length);
        System.arraycopy(b, 0, r, a.length, b.length);
		return r;
	}
	

}
