package hr.fer.nenr.projekt.DZ1.zadatak1;


public interface IDomain extends Iterable<DomainElement> {

	public int getCardinality();
	
	public IDomain getComponent(int component);
	
	public int getNumberOfComponent();
	
	public int indexOfElement(DomainElement element);
	
	public DomainElement elementForIndex(int index);
	
}
