package hr.fer.nenr.projekt.DZ1.zadatak2;

public interface IIntUnaryFunction {

	public double valueAt(int i);

}
