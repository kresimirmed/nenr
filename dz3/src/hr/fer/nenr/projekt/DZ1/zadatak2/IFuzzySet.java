package hr.fer.nenr.projekt.DZ1.zadatak2;

import hr.fer.nenr.projekt.DZ1.zadatak1.*;

public interface IFuzzySet {

	public IDomain getDomain();

	public double getValueAt(DomainElement element);
}
