package hr.fer.nenr.projekt.DZ1.zadatak2;

import hr.fer.nenr.projekt.DZ1.zadatak1.*;

public class MutableFuzzySet implements IFuzzySet {

	private double[] memberships;
	private IDomain domain;

	public MutableFuzzySet(IDomain domain) {
		this.domain = domain;
		memberships = new double[domain.getCardinality()];
	}

	@Override
	public IDomain getDomain() {
		return domain;
	}

	@Override
	public double getValueAt(DomainElement element) {
		int index = domain.indexOfElement(element);
		return memberships[index];
	}

	public MutableFuzzySet set(DomainElement element, double mu) {
		int index = domain.indexOfElement(element);
		memberships[index] = mu;
		return this;
	}

}
