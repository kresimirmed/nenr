package hr.fer.nenr.projekt.DZ1.zadatak3;

public interface IBinaryFunction {

	public double valueAt(double x, double y);

}
