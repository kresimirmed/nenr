package hr.fer.nenr.projekt.DZ3.brodic;

import hr.fer.nenr.projekt.DZ1.zadatak3.IBinaryFunction;
import hr.fer.nenr.projekt.DZ3.base.AkcelBase;
import hr.fer.nenr.projekt.DZ3.base.RuleBase;

public class AkcelFuzzySystemMin implements FuzzySystem {

	private Defuzzifier defuzzifier;
	private RuleBase akcel;

	public AkcelFuzzySystemMin(Defuzzifier defuzzifier, IBinaryFunction funAnd, IBinaryFunction funcOr) {
		this.defuzzifier = defuzzifier;
		akcel = new AkcelBase(funAnd, funcOr);
	}

	@Override
	public int conclude(int... p) {
		return defuzzifier.defuzzification(akcel.getConclusionFromRules(p));
	}
}
