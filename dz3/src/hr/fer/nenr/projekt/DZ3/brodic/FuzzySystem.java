package hr.fer.nenr.projekt.DZ3.brodic;


public interface FuzzySystem {

    int conclude(int... p);

}
