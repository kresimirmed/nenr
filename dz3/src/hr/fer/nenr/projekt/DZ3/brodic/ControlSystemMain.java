package hr.fer.nenr.projekt.DZ3.brodic;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import hr.fer.nenr.projekt.DZ1.zadatak3.IBinaryFunction;
import hr.fer.nenr.projekt.DZ1.zadatak3.Operations;

public class ControlSystemMain {

	public static void main(String[] args) throws IOException {

		Defuzzifier def = new Defuzzifier();

		IBinaryFunction funAnd = (x, y) -> x * y;
		IBinaryFunction funOR = Operations.zadehOr();
		FuzzySystem fAkcel = new AkcelFuzzySystemMin(def, funAnd, funOR);
		FuzzySystem fKorm = new KormiloFuzzySystemMin(def, funAnd, funOR);

		BufferedReader input = new BufferedReader(new InputStreamReader(System.in));

		int L = 0, D = 0, LK = 0, DK = 0, V = 0, S = 0;
		String line = null;
		while (true) {
			if ((line = input.readLine()) != null) {
				if (line.contains("KRAJ"))
					break;
				String[] p = line.trim().split("\\s+");
				L = Integer.valueOf(p[0]);
				D = Integer.valueOf(p[1]);
				LK = Integer.valueOf(p[2]);
				DK = Integer.valueOf(p[3]);
				V = Integer.valueOf(p[4]);
				S = Integer.valueOf(p[5]);
			}

			System.out.println(fAkcel.conclude(L, D, LK, DK, V, S) + " " + fKorm.conclude(L, D, LK, DK, V, S));
			System.out.flush();
		}
	}
}
