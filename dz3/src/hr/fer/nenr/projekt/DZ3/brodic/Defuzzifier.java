package hr.fer.nenr.projekt.DZ3.brodic;

import hr.fer.nenr.projekt.DZ1.zadatak1.DomainElement;
import hr.fer.nenr.projekt.DZ1.zadatak1.IDomain;
import hr.fer.nenr.projekt.DZ1.zadatak2.IFuzzySet;

public class Defuzzifier {

	public Defuzzifier() {
	}

	public int defuzzification(IFuzzySet set) {
		double w = 0;
		double m = 0;
		IDomain d = set.getDomain();
		for (int i = 0, size = d.getCardinality(); i < size; i++) {
			DomainElement ele = d.elementForIndex(i);
			double member = set.getValueAt(ele);
			w += member * ele.getComponentValue(0);
			m += member;
		}
		double v = w / m;
		return (int) Math.round(v);
	}
}