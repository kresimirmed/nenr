package hr.fer.nenr.projekt.DZ3.brodic;

import hr.fer.nenr.projekt.DZ1.zadatak3.IBinaryFunction;
import hr.fer.nenr.projekt.DZ3.base.KorimBase;
import hr.fer.nenr.projekt.DZ3.base.RuleBase;

public class KormiloFuzzySystemMin implements FuzzySystem {

    private Defuzzifier defuzzifier;
    private RuleBase korm;

    public KormiloFuzzySystemMin(Defuzzifier defuzzifier, IBinaryFunction funAnd,IBinaryFunction funcOr) {
        this.defuzzifier = defuzzifier;
        korm = new KorimBase(funAnd,funcOr);
    }

    @Override
    public int conclude(int... p) {
        return defuzzifier.defuzzification(korm.getConclusionFromRules(p));
    }
}
