package hr.fer.nenr.projekt.DZ3.base;

import hr.fer.nenr.projekt.DZ1.zadatak1.IDomain;
import hr.fer.nenr.projekt.DZ1.zadatak1.SimpleDomain;
import hr.fer.nenr.projekt.DZ1.zadatak2.CalculatedFuzzySet;
import hr.fer.nenr.projekt.DZ1.zadatak2.IFuzzySet;
import hr.fer.nenr.projekt.DZ1.zadatak2.StandardFuzzySets;
import hr.fer.nenr.projekt.DZ1.zadatak3.IBinaryFunction;

public class AkcelBase extends RuleBase {

	private IBinaryFunction func;

	public AkcelBase(IBinaryFunction functionAnd, IBinaryFunction functionOr) {

		super(functionOr);
		this.func = functionAnd;

		IDomain akcel = new SimpleDomain(-20, 21);
		IFuzzySet faster = new CalculatedFuzzySet(akcel, StandardFuzzySets.gammaFunction(25, 33));
		IFuzzySet mFaster = new CalculatedFuzzySet(akcel, StandardFuzzySets.lambdaFunction(10, 20, 30));
		IFuzzySet slower = new CalculatedFuzzySet(akcel, StandardFuzzySets.IFunction(7, 15));

		IDomain v = new SimpleDomain(0, 201);
		IFuzzySet dSlow = new CalculatedFuzzySet(v, StandardFuzzySets.IFunction(10, 20));
		IFuzzySet slow = new CalculatedFuzzySet(v, StandardFuzzySets.lambdaFunction(15, 30, 45));
		IFuzzySet normal = new CalculatedFuzzySet(v, StandardFuzzySets.lambdaFunction(40, 60, 80));
		IFuzzySet fast = new CalculatedFuzzySet(v, StandardFuzzySets.lambdaFunction(75, 90, 105));
		IFuzzySet dFast = new CalculatedFuzzySet(v, StandardFuzzySets.gammaFunction(100, 120));

		IDomain sd = new SimpleDomain(0, 1301);
		IFuzzySet JB = new CalculatedFuzzySet(sd, StandardFuzzySets.IFunction(20, 30));
		IFuzzySet B = new CalculatedFuzzySet(sd, StandardFuzzySets.lambdaFunction(25, 45, 65));
		IFuzzySet D = new CalculatedFuzzySet(sd, StandardFuzzySets.lambdaFunction(50, 70, 90));
		IFuzzySet JD = new CalculatedFuzzySet(sd, StandardFuzzySets.gammaFunction(80, 110));

		IDomain s = new SimpleDomain(0, 2);
		IFuzzySet ok = new CalculatedFuzzySet(s, StandardFuzzySets.gammaFunction(0, 1));

		Rule r1 = new Rule(func, new int[] { 3, 2, 2, 2, 2, 0 }, B, D, JD, JB, B, D, JD, JB, B, dSlow, slow, ok,
				faster);
		rules.add(r1);

		Rule r2 = new Rule(func, new int[] { 0, 0, 3, 3, 2, 0 }, B, B, B, D, JD, B, D, JD, dSlow, slow, ok, faster);
		rules.add(r2);

		Rule r3 = new Rule(func, new int[] { 3, 0, 2, 0, 2, 0 }, B, D, JD, JB, D, JD, JB, dSlow, slow, normal, ok,
				faster);
		rules.add(r3);

		Rule r4 = new Rule(func, new int[] { 2, 3, 2, 2, 2, 0 }, JB, B, B, D, JD, JB, B, D, JD, dSlow, slow, ok,
				faster);
		rules.add(r4);

		Rule r5 = new Rule(func, new int[] { 2, 2, 2, 2, 0, 0 }, B, D, B, D, B, D, B, D, normal, ok, mFaster);
		rules.add(r5);

		Rule r6 = new Rule(func, new int[] { 0, 0, 3, 3, 2, 0 }, B, B, B, D, JD, B, D, JD, dFast, fast, ok, slower);
		rules.add(r6);

		Rule r7 = new Rule(func, new int[] { 2, 2, 2, 2, 2, 0 }, D, JD, D, JD, D, JD, D, JD, slow, dSlow, ok, faster);
		rules.add(r7);

		Rule r8 = new Rule(func, new int[] { 0, 3, 0, 2, 2, 0 }, JB, B, D, JD, JB, D, JD, dSlow, slow, ok, faster);
		rules.add(r8);

	}

}
