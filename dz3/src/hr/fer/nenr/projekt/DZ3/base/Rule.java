package hr.fer.nenr.projekt.DZ3.base;

import hr.fer.nenr.projekt.DZ1.zadatak1.DomainElement;
import hr.fer.nenr.projekt.DZ1.zadatak1.IDomain;
import hr.fer.nenr.projekt.DZ1.zadatak2.IFuzzySet;
import hr.fer.nenr.projekt.DZ1.zadatak2.MutableFuzzySet;
import hr.fer.nenr.projekt.DZ1.zadatak3.IBinaryFunction;
import hr.fer.nenr.projekt.DZ1.zadatak3.Operations;

public class Rule {

	private IFuzzySet[] relation;
	private IBinaryFunction binaryFunction;
	private int[] operations;

	public Rule(IBinaryFunction function, int[] operations, IFuzzySet... relation) {
		super();
		this.relation = relation;
		this.binaryFunction = function;
		this.operations = operations;
	}

	public IFuzzySet compute(int[] parameters) {
		IDomain konzekvens = relation[relation.length - 1].getDomain();
		double minValue = 1.0;
		for (int i = 0, j = 0; i < operations.length - 1; i++) {
			double tmpVal = relation[j].getValueAt(DomainElement.of(parameters[i]));
			if (operations[i] > 0) {
				int t = operations[i];
				double max = 0;
				while (t > 0) {
					t--;
					max = Operations.zadehOr().valueAt(max, relation[j++].getValueAt(DomainElement.of(parameters[i])));
				}
				minValue = binaryFunction.valueAt(minValue, max);
			} else {
				minValue = binaryFunction.valueAt(minValue,
						operations[i] == -1 ? Operations.zadehNot().valueAt(tmpVal) : tmpVal);
				j++;
			}
		}
		MutableFuzzySet newSet = new MutableFuzzySet(konzekvens);
		if (minValue != 0.0)
			for (int i = 0, kard = konzekvens.getCardinality(); i < kard; i++) {
				DomainElement de = konzekvens.elementForIndex(i);
				newSet.set(de, binaryFunction.valueAt(relation[relation.length - 1].getValueAt(de), minValue));
			}
		return newSet;
	}

}
