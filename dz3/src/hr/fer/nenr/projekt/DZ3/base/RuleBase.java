package hr.fer.nenr.projekt.DZ3.base;

import java.util.LinkedList;
import java.util.List;

import hr.fer.nenr.projekt.DZ1.zadatak2.IFuzzySet;
import hr.fer.nenr.projekt.DZ1.zadatak3.IBinaryFunction;
import hr.fer.nenr.projekt.DZ1.zadatak3.Operations;

public class RuleBase {

	protected List<Rule> rules = new LinkedList<>();
	protected IBinaryFunction function;

	public RuleBase(IBinaryFunction function) {
		this.function = function;
	}

	public IFuzzySet getConclusionFromRules(int[] paramters) {
		IFuzzySet newSet = rules.get(0).compute(paramters);
		for (int i = 1; i < rules.size(); i++) {
			newSet = Operations.binaryOperation(newSet, rules.get(i).compute(paramters), function);
		}
		return newSet;
	}
}
