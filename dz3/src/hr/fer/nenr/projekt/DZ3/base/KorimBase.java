package hr.fer.nenr.projekt.DZ3.base;

import hr.fer.nenr.projekt.DZ1.zadatak1.IDomain;
import hr.fer.nenr.projekt.DZ1.zadatak1.SimpleDomain;
import hr.fer.nenr.projekt.DZ1.zadatak2.CalculatedFuzzySet;
import hr.fer.nenr.projekt.DZ1.zadatak2.IFuzzySet;
import hr.fer.nenr.projekt.DZ1.zadatak2.StandardFuzzySets;
import hr.fer.nenr.projekt.DZ1.zadatak3.IBinaryFunction;

public class KorimBase extends RuleBase {

	private IBinaryFunction func;

	public KorimBase(IBinaryFunction functionAnd, IBinaryFunction functionOr) {

		super(functionOr);
		this.func = functionAnd;

		IDomain kormilo = new SimpleDomain(-90, 90);
		IFuzzySet sharpR = new CalculatedFuzzySet(kormilo, StandardFuzzySets.IFunction(10, 30));
		IFuzzySet right = new CalculatedFuzzySet(kormilo, StandardFuzzySets.lambdaFunction(20, 40, 60));
		IFuzzySet left = new CalculatedFuzzySet(kormilo, StandardFuzzySets.lambdaFunction(120, 140, 160));
		IFuzzySet sharpL = new CalculatedFuzzySet(kormilo, StandardFuzzySets.gammaFunction(150, 170));

		IDomain v = new SimpleDomain(0, 201);
		IFuzzySet slow = new CalculatedFuzzySet(v, StandardFuzzySets.IFunction(10, 20));
		IFuzzySet fast = new CalculatedFuzzySet(v, StandardFuzzySets.gammaFunction(100, 120));

		IDomain s = new SimpleDomain(0, 2);
		IFuzzySet ok = new CalculatedFuzzySet(s, StandardFuzzySets.gammaFunction(0, 1));
		IFuzzySet notOk = new CalculatedFuzzySet(s, StandardFuzzySets.IFunction(1, 0));
		
		IDomain l = new SimpleDomain(0, 1301);
		IFuzzySet JB = new CalculatedFuzzySet(l, StandardFuzzySets.IFunction(20, 30));
		IFuzzySet B = new CalculatedFuzzySet(l, StandardFuzzySets.lambdaFunction(20, 45, 70));
		IFuzzySet D = new CalculatedFuzzySet(l, StandardFuzzySets.lambdaFunction(50, 70, 90));
		IFuzzySet JD = new CalculatedFuzzySet(l, StandardFuzzySets.gammaFunction(80, 100));


		Rule r1 = new Rule(func, new int[] { 0, 0, 0, 2, -1, 0 }, JD, JD, JD, B, D, fast, ok, left);
		rules.add(r1);

		Rule r2 = new Rule(func, new int[] { 0, 0, 0, 0, -1, 0 }, JD, JD, JD, D, fast, ok, left);
		rules.add(r2);

		Rule r3 = new Rule(func, new int[] { 0, 0, 0, 0, -1, 0 }, JD, JD, D, JD, fast, ok, right);
		rules.add(r3);

		Rule r4 = new Rule(func, new int[] { 2, 0, 0, 2, -1, 0 }, B, D, JD, JD, JB, B, fast, ok, left);
		rules.add(r4);

		Rule r5 = new Rule(func, new int[] { 0, 3, 2, 3, -1, 0 }, JB, B, D, JD, JB, B, B, D, JD, fast, ok, sharpR);
		rules.add(r5);

		Rule r6 = new Rule(func, new int[] { 3, 0, 3, 2, -1, 0 }, B, D, JD, JB, B, D, JD, JB, B, fast, ok,
				sharpL);
		rules.add(r6);

		Rule r7 = new Rule(func, new int[] { 0, 3, 2, 3, -1, 0 }, B, B, D, JD, JB, B, B, D, JD, fast, ok, sharpR);
		rules.add(r7);

		Rule r8 = new Rule(func, new int[] { 0, 2, 0, 2, -1, 0 }, JB, D, JD, JB, D, JD, fast, ok, sharpR);
		rules.add(r8);

		Rule r9 = new Rule(func, new int[] { 2, 0, 2, 0, -1, 0 }, D, JD, JB, D, JD, JB, fast, ok, sharpL);
		rules.add(r9);

		Rule r10 = new Rule(func, new int[] { 0, -1, -1, -1, -1, 0 }, JB, JB, JB, JB, fast, ok, sharpR);
		rules.add(r10);

		Rule r11 = new Rule(func, new int[] { -1, 0, -1, -1, -1, 0 }, JB, JB, JB, JB, fast, ok, sharpL);
		rules.add(r11);

		Rule r12 = new Rule(func, new int[] { -1, -1, 0, -1, -1, 0 }, JB, JB, JB, JB, fast, ok, sharpR);
		rules.add(r12);

		Rule r13 = new Rule(func, new int[] { 3, 0, 3, 2, -1, 0 }, B, D, JD, B, B, D, JD, JB, B, fast, ok,
				sharpL);
		rules.add(r13);

		Rule r14 = new Rule(func, new int[] { 2, 3, 0, 3, -1, 0 }, JB, B, B, D, JD, JB, B, D, JD, fast, ok,
				sharpR);
		rules.add(r14);

		Rule r15 = new Rule(func, new int[] { 3, 2, 3, 0, -1, 0 }, B, D, JD, JB, B, B, D, JD, JB, fast, ok,
				sharpL);
		rules.add(r15);

		Rule r16 = new Rule(func, new int[] { 2, 3, 0, 3, -1, 0 }, JB, B, B, D, JD, B, B, D, JD, fast, ok, sharpR);
		rules.add(r16);

		Rule r17 = new Rule(func, new int[] { 3, 2, 3, 0, -1, 0 }, B, D, JD, JB, B, B, D, JD, B, fast, ok,
				sharpL);
		rules.add(r17);

		Rule r18 = new Rule(func, new int[] { 2, 3, 2, 3, -1, 0 }, JB, B, B, D, JD, JB, B, B, D, JD, fast, ok,
				sharpR);
		rules.add(r18);

		Rule r19 = new Rule(func, new int[] { 3, 2, 3, 2, -1, 0 }, B, D, JD, JB, B, B, D, JD, JB, B, fast, ok,
				sharpL);
		rules.add(r19);

		Rule r20 = new Rule(func, new int[] { 3, 0, 2, 0, -1, 0 }, JB, B, D, JD, JB, B, JD, fast, ok, sharpR);
		rules.add(r20);

		Rule r21 = new Rule(func, new int[] { 0, 3, 0, 2, -1, 0 }, JD, JB, B, D, JD, JB, B, fast, ok, sharpL);
		rules.add(r21);

		Rule r22 = new Rule(func, new int[] { 0, 2, 3, 2, -1, 0 }, JB, D, JD, B, D, JD, D, JD, fast, notOk,
				sharpR);
		rules.add(r22);

		Rule r23 = new Rule(func, new int[] { 2, 0, 2, 3, -1, 0 }, D, JD, JB, D, JD, B, D, JD, fast, notOk,
				sharpL);
		rules.add(r23);

		Rule r24 = new Rule(func, new int[] { 0, -1, -1, -1, -1, 0 }, JB, JB, JB, JB, fast, ok, sharpR);
		rules.add(r24);

		Rule r25 = new Rule(func, new int[] { -1, 0, -1, -1, -1, 0 }, JB, JB, JB, JB, fast, ok, sharpL);
		rules.add(r25);

	}

}
