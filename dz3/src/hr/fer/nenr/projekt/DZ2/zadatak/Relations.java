package hr.fer.nenr.projekt.DZ2.zadatak;

import java.util.ArrayList;
import java.util.Collections;

import hr.fer.nenr.projekt.DZ1.zadatak1.DomainElement;
import hr.fer.nenr.projekt.DZ1.zadatak1.IDomain;
import hr.fer.nenr.projekt.DZ1.zadatak2.IFuzzySet;
import hr.fer.nenr.projekt.DZ1.zadatak1.Domain;

public class Relations {

	public Relations() {

	}

	public static boolean isSymmetric(IFuzzySet relation) {
		if (!isUtimesURelation(relation))
			return false;
		IDomain domain = relation.getDomain();
		int nOfComp = domain.getNumberOfComponent();
		for (DomainElement e : domain) {
			int[] pom = new int[nOfComp];
			int i = nOfComp;
			for (int x : e.getValues()) {
				pom[--i] = x;
			}
			if (Double.compare(relation.getValueAt(DomainElement.of(pom)), relation.getValueAt(e)) != 0)
				return false;
		}
		return true;
	}

	public static boolean isReflexive(IFuzzySet relation) {
		if (!isUtimesURelation(relation))
			return false;
		IDomain domain = relation.getDomain();
		ArrayList<int[]> diagonal = returnDiagonal(domain);
		for (int[] x : diagonal) {
			if (relation.getValueAt(DomainElement.of(x)) != 1)
				return false;
		}
		return true;
	}

	public static boolean isMaxMinTransitive(IFuzzySet relation) {
		if (!isUtimesURelation(relation))
			return false;
		IDomain domain = relation.getDomain();
		int[] elements = returnElements(domain);
		for (int x : elements) {
			for (int z : elements) {
				ArrayList<Double> list = new ArrayList<>();
				for (int y : elements) {
					list.add(Double.min(relation.getValueAt(DomainElement.of(x, y)),
							relation.getValueAt(DomainElement.of(y, z))));
				}
				double max = list.get(0);
				for (double i : list) {
					if (max < i)
						max = i;
				}
				if (relation.getValueAt(DomainElement.of(x, z)) < max)
					return false;
			}
		}
		return true;
	}

	public static IFuzzySet compositionOfBinaryRelations(IFuzzySet relation1, IFuzzySet relation2) {
		IDomain xDomain = relation1.getDomain().getComponent(0);
		IDomain yDomain = relation1.getDomain().getComponent(1);
		IDomain zDomain = relation2.getDomain().getComponent(1);
		IDomain compositeDomain = Domain.combine(xDomain, zDomain);
		return new IFuzzySet() {

			@Override
			public double getValueAt(DomainElement element) {
				ArrayList<Double> mins = new ArrayList<Double>();
				for (int i = 0; i < yDomain.getCardinality(); i++) {
					double value1 = relation1.getValueAt(DomainElement.of(element.getComponentValue(0), yDomain.elementForIndex(i).getValues()[0]));
					double value2 = relation2.getValueAt(DomainElement.of(yDomain.elementForIndex(i).getValues()[0], element.getComponentValue(1)));
					mins.add(Math.min(value1, value2));
				}
				return Collections.max(mins);
			}

			@Override
			public IDomain getDomain() {
				return compositeDomain;
			}
		};
	}

	public static boolean isFuzzyEquivalence(IFuzzySet relation) {
		return isSymmetric(relation) && isMaxMinTransitive(relation) && isReflexive(relation);
	}

	public static boolean isUtimesURelation(IFuzzySet relation) {
		IDomain domain = relation.getDomain();
		if (domain.getNumberOfComponent() != 2)
			return false;
		ArrayList<Integer> stupci = new ArrayList<>();
		ArrayList<Integer> retci = new ArrayList<>();
		for (DomainElement e : domain) {
			int redak = e.getValues()[0];
			int stupac = e.getValues()[1];
			if (!stupci.contains(new Integer(stupac)))
				stupci.add(new Integer(stupac));
			if (!retci.contains(new Integer(redak)))
				retci.add(new Integer(redak));
		}
		for (Integer x : stupci) {
			if (!retci.contains(x))
				return false;
		}
		for (Integer x : retci) {
			if (!stupci.contains(x))
				return false;
		}
		return true;
	}

	private static ArrayList<int[]> returnDiagonal(IDomain domain) {
		ArrayList<int[]> diagonal = new ArrayList<>();
		for (DomainElement e : domain) {
			int[] ele = e.getValues();
			if (ele[0] == ele[1])
				diagonal.add(ele);
		}
		return diagonal;
	}

	private static int[] returnElements(IDomain domain) {
		ArrayList<int[]> diagonal = returnDiagonal(domain);
		int listSize = diagonal.size();
		int[] returnList = new int[listSize];
		for (int i = 0; i < listSize; ++i) {
			returnList[i] = diagonal.get(i)[0];
		}
		return returnList;
	}


}
