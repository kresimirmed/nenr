package hr.fer.nenr.projekt.DZ1.zadatak1;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class SimpleDomain extends Domain {

	private int first;
	private int last;
	
	private List<DomainElement> elements=new ArrayList<>();
	
	public SimpleDomain(int first,int last) {
		this.first=first;
		this.last=last;
		
		for (int i=first;i<last;++i) {
			elements.add(new DomainElement(new int[] {i}));
		}
	}

	@Override
	public int getCardinality() {
		return last-first;
	}

	@Override
	public IDomain getComponent(int component) {
		return this;
	}

	@Override
	public int getNumberOfComponent() {
		return 1;
	}
	
	public int getFirst() {
		return first;
	}
	
	public int getLast() {
		return last;
	}
	
	public Iterator<DomainElement> iterator(){
		return new SDIterator();
	}
	
	private class SDIterator implements Iterator<DomainElement>{
		
		private int current;
		
		public SDIterator() {
			current=first;
		}

		@Override
		public boolean hasNext() {
			// TODO Auto-generated method stub
			return current<last;
		}

		@Override
		public DomainElement next() {
			return new DomainElement(new int[] {current++});
		}
		
	}

	@Override
	public int indexOfElement(DomainElement element) {
		for (int i=0;i<elements.size();++i) {
			if(element.equals(elements.get(i))) return i;
		}
		return -1;
	}

	@Override
	public DomainElement elementForIndex(int index) {
		// TODO Auto-generated method stub
		return elements.get(index);
	}
	
	public List<DomainElement> getDomainElement(){
		return elements;
	}
	
	
	
}
