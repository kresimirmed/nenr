package hr.fer.nenr.projekt.DZ1.zadatak1;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;



public class CompositeDomain extends Domain {

	private SimpleDomain[] simpleDomains;
	private DomainElement[] domainElements;

	public CompositeDomain(SimpleDomain[] domains) {
		this.simpleDomains = domains;

		List<DomainElement> pom = cartezian();
		this.domainElements = new DomainElement[pom.size()];
		for (int i = 0; i<domainElements.length;++i) {
			domainElements[i]=new DomainElement(pom.get(i).getValues());
		}
		
		
	}

	private List<DomainElement> cartezian() {
		if (simpleDomains.length <= 1)
			return simpleDomains[0].getDomainElement();
		return processCartezian(0);
	}
	
	private List<DomainElement> processCartezian(int i){
		List<DomainElement> list = new ArrayList<>();
		
		if(i==simpleDomains.length) {
			list.add(new DomainElement(new int[0]));
			return list;
		}else {
			for(DomainElement x: simpleDomains[i]) {
				for(DomainElement y: processCartezian(i+1)) {
					int xv = x.getValues().length;
					int yv = y.getValues().length;
					int[] array = new int[xv+yv];
					
					System.arraycopy(x.getValues(),	0, array, 0,xv);
					System.arraycopy(y.getValues(), 0, array, xv, yv);
					
					list.add(new DomainElement(array));
				}
			}
			return list;
		}
	}

	@Override
	public int getCardinality() {
		int cardinality = 1;
		for (SimpleDomain x : simpleDomains) {
			cardinality = cardinality * x.getCardinality();
		}
		return cardinality;
	}

	@Override
	public IDomain getComponent(int component) {
		// TODO Auto-generated method stub
		return simpleDomains[component];
	}

	@Override
	public int getNumberOfComponent() {
		// TODO Auto-generated method stub
		return simpleDomains.length;
	}

	public SimpleDomain[] getSimpleDomain() {
		return simpleDomains;
	}

	@Override
	public int indexOfElement(DomainElement element) {
		for (int i=0; i<domainElements.length;++i) {
			if(element.equals(domainElements[i])) return i;
		}
		return -1;
	}

	@Override
	public DomainElement elementForIndex(int index) {
		// TODO Auto-generated method stub
		return domainElements[index];
	}

	@Override
	public Iterator<DomainElement> iterator() {
		return new Iterator<DomainElement>() {
			
			private int index = 0;
			private DomainElement currentElement = domainElements[index];
			
			@Override
			public boolean hasNext() {
				if (index < domainElements.length) {
					return true;
				} else {
					return false;
				}
			}

			@Override
			public DomainElement next() {
				currentElement = domainElements[index++];
				return currentElement;
			}
		};
	}

}
