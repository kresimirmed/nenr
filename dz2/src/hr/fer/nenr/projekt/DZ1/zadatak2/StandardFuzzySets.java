package hr.fer.nenr.projekt.DZ1.zadatak2;

public class StandardFuzzySets {

	public StandardFuzzySets() {

	}

	public static IIntUnaryFunction IFunction(int a, int b) {
		return new IIntUnaryFunction() {

			@Override
			public double valueAt(int i) {
				if (i < a)
					return 1;
				else if (i >= a && i < b)
					return ((double) (b - i) / (double) (b - a));
				else
					return 0;

			}
		};
	}

	public static IIntUnaryFunction gammaFunction(int a, int b) {
		return new IIntUnaryFunction() {

			@Override
			public double valueAt(int i) {
				if (i < a)
					return 0;
				else if (i >= a && i < b)
					return ((double) (i - b) / (double) (b - a));
				else
					return 1;
			}
		};
	}

	public static IIntUnaryFunction lambdaFunction(int a, int b, int g) {
		return new IIntUnaryFunction() {

			@Override
			public double valueAt(int i) {
				if (i < a)
					return 0;
				else if (i >= a && i < b)
					return ((double) (i - a) / (double) (b - a));
				else if (i >= b && i < g)
					return ((double) (g - i) / (double) (g - b));
				else
					return 0;

			}
		};
	}

}
