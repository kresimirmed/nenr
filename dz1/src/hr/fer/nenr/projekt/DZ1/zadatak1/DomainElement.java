package hr.fer.nenr.projekt.DZ1.zadatak1;

import java.util.Arrays;

public class DomainElement {

	private int[] values;

	public DomainElement(int[] values) {
		this.values = values;
	}

	public int getNumberOfComponents() {
		return values.length;
	}

	public int getComponentValue(int index) {
		if (index >= values.length) {
			System.out.println("Index out of range!!\n You will get first element!!!");
			return values[0];
		}
		return values[index];
	}

	public int hashCode() {
		return values.hashCode();
	}

	public boolean equals(Object element) {
		if (element instanceof DomainElement && Arrays.equals(((DomainElement) element).values, this.values)) {
			return true;
		}
		return false;
	}

	public String toString() {
		if (getNumberOfComponents() == 1) {
			return Integer.toString(values[0]);
		} else {
			String string = "(";
			int length = getNumberOfComponents();
			for (int i = 0; i < length; i++) {
				if (i != length - 1) {
					string += Integer.toString(values[i]) + ",";
				} else {
					string += Integer.toString(values[i]);
				}
			}
			string += ")";
			return string;
		}
	}

	public static DomainElement of(int... values) {
		return new DomainElement(values);
	}

	public int[] getValues() {
		return values;
	}

}
