package hr.fer.nenr.projekt.DZ1.zadatak1;

import hr.fer.nenr.projekt.DZ1.zadatak2.IFuzzySet;

public class Debug {

	public static void print(IDomain domain, String headingText) {

		if (headingText != null) {
			System.out.println(headingText);
		}

		for (DomainElement e : domain) {
			System.out.println("Element domene: " + e);
		}

		System.out.println("Kardinalitet domene je: " + domain.getCardinality());
		System.out.println();
	}

	public static void print(IFuzzySet set, String headingText) {

		if (headingText != null) {
			System.out.println();
			System.out.println(headingText);
		}

		IDomain domain = set.getDomain();

		for (DomainElement e : domain) {
			System.out.format("d(%d)=%.6f\n", domain.indexOfElement(e), set.getValueAt(e));
		}
	}
}
