package hr.fer.nenr.projekt.DZ1.zadatak2;

import hr.fer.nenr.projekt.DZ1.zadatak1.*;

public class CalculatedFuzzySet implements IFuzzySet {

	private IDomain domain;
	private IIntUnaryFunction func;

	public CalculatedFuzzySet(IDomain domain, IIntUnaryFunction func) {
		this.domain = domain;
		this.func = func;
	}

	@Override
	public IDomain getDomain() {
		return domain;
	}

	@Override
	public double getValueAt(DomainElement element) {
		int x = domain.indexOfElement(element);
		return func.valueAt(x);
	}

}
