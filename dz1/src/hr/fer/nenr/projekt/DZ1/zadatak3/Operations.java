package hr.fer.nenr.projekt.DZ1.zadatak3;

import hr.fer.nenr.projekt.DZ1.zadatak1.*;
import hr.fer.nenr.projekt.DZ1.zadatak2.*;

public class Operations {

	public Operations() {

	}

	public static IFuzzySet unaryOperation(IFuzzySet iFuzzySet, IUnaryFunction function) {
		return new IFuzzySet() {

			@Override
			public double getValueAt(DomainElement element) {
				double index = iFuzzySet.getValueAt(element);
				return function.valueAt(index);
			}

			@Override
			public IDomain getDomain() {
				// TODO Auto-generated method stub
				return iFuzzySet.getDomain();
			}
		};
	}

	public static IFuzzySet binaryOperation(IFuzzySet iFuzzySet1, IFuzzySet iFuzzySet2, IBinaryFunction function) {
		return new IFuzzySet() {

			@Override
			public double getValueAt(DomainElement element) {
				return function.valueAt(iFuzzySet1.getValueAt(element), iFuzzySet2.getValueAt(element));
			}

			@Override
			public IDomain getDomain() {
				return iFuzzySet1.getDomain();
			}
		};
	}

	public static IUnaryFunction zadehNot() {
		return new IUnaryFunction() {

			@Override
			public double valueAt(double x) {
				return (double) (1 - x);
			}
		};
	}

	public static IBinaryFunction zadehAnd() {
		return new IBinaryFunction() {

			@Override
			public double valueAt(double x, double y) {
				return Math.min(x, y);
			}
		};
	}

	public static IBinaryFunction zadehOr() {
		return new IBinaryFunction() {

			@Override
			public double valueAt(double x, double y) {
				return Math.max(x, y);
			}
		};
	}

	public static IBinaryFunction hamacherTNorm(double i) {
		return new IBinaryFunction() {

			@Override
			public double valueAt(double x, double y) {
				return (double) (x * y) / (double) (i + (1 - i) * (x + y - x * y));
			}
		};
	}

	public static IBinaryFunction hamacherSNorm(double x) {
		return new IBinaryFunction() {

			@Override
			public double valueAt(double x, double y) {
				return (double) (x + y - 2 * x * y) / (double) (1 - x * y);
			}
		};
	}

}
