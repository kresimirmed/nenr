package hr.fer.nenr.projekt.DZ1.zadatak3;

public interface IUnaryFunction {

	public double valueAt(double x);

}
