package hr.fer.zemris.nenr.dz4;

public class Population {

	private Individual[] elements;
	
	public Population (int populationSize) {
		elements = new Individual[populationSize];
		
		for (int i = 0; i<populationSize;++i) {
			Individual newE = new Individual();
			newE.generateFaktors();
			elements[i]= newE;
		}
	}
	
	public Individual getElementOnIndex(int index) {
		return elements[index];
	}
	
	public Individual[] getElements() {
		return elements;
	}
	
	public int size() {
		return elements.length;
	}
	
	public void save(int index, Individual ind) {
		elements[index]= ind;
	}
}
