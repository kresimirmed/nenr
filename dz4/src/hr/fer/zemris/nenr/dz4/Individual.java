package hr.fer.zemris.nenr.dz4;

import java.util.Random;

public class Individual {

	public static final int LENGHT = 4;
	public static final int MAX = 4;
	public static final int MIN = -4;
	private double[] faktors;

	public Individual() {
		faktors = new double[LENGHT];
		
	}

	public void generateFaktors() {
		Random r = new Random();
		for (int i = 0; i<LENGHT;i++) {
			faktors[i]= MIN +(MAX-MIN)*r.nextDouble();
		}
	}
	
	public double getFaktorOnIdex(int index) {
		if (index >= LENGHT || index < 0)
			System.err.println("Index nije dobar, mora biti u intervalu [0," + LENGHT + ")");
		return faktors[index];

	}

	public double[] getFaktors() {
		return faktors;
	}
	
	public void setFaktorOnIndex(int index, double faktor) {
		faktors[index]=faktor;
	}
	
	/*
	 * Roditelj je trenutni faktori i primljeni faktori
	 * Vraćamo dijete
	 */
	public Individual crossing (Individual parent2) {
		Individual child = new Individual();
		//Kako od kojeg roditelja TODO
		//Možda bolje u algoritham klasi
		
		return child;
	}

}
